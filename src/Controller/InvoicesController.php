<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Http\Session;

/**
 * Invoices Controller
 *
 * @property \App\Model\Table\InvoicesTable $Invoices
 *
 * @method \App\Model\Entity\Invoice[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InvoicesController extends AppController
{

    var $helpers = array('Time');
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->set('title', 'Faktury');
        $invoices = $this->paginate($this->Invoices, ['limit' => 5]);
        if($this->request->is('ajax')){
            $this->set(compact('invoices'));
            $this->set('_serialize', ['invoices']);
        }else{
            $this->set(compact('invoices'));
        }

    }

    /**
     * ajaxIndex method
     *
     * @return \Cake\Http\Response|null
     */
    public function ajaxIndex()
    {
        $this->set('title', 'Faktury');
        $countRecords = $this->request->getData('length');
        $start = $this->request->getData('start');

        $orderColumns = $this->request->getData('order');
        $orders = [];
        foreach($orderColumns as $orderColumn){
            $orders[$this->request->getData('columns')[$orderColumn['column']]['data']] = $orderColumn['dir'];
        }

        $count = $this->Invoices->find()->count();
        $invoices = $this->Invoices->find()->order($orders)->offset($start)->limit($countRecords);

        if($this->request->is('ajax')){
            $this->set(compact('invoices'));
            $this->set('recordsTotal', $count);
            $this->set('recordsFiltered', $count);
            $this->set('_serialize', ['recordsTotal', 'recordsFiltered','invoices']);
        }else{
            $this->set(compact('invoices'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Invoice id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $invoice = $this->Invoices->get($id, [
            'contain' => []
        ]);

        $this->set('title', 'Faktura nr: '.$invoice->number);

        $this->set('invoice', $invoice);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nowa Faktura');

        $invoice = $this->Invoices->newEntity();
        if ($this->request->is('post')) {
            $invoice = $this->Invoices->patchEntity($invoice, $this->request->getData());
            if ($this->Invoices->save($invoice)) {
                $this->Flash->success(__('The invoice has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The invoice could not be saved. Please, try again.'));
        }
        $this->set(compact('invoice'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Invoice id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $invoice = $this->Invoices->get($id, [
            'contain' => []
        ]);
        $this->set('title', 'Edycja faktury nr: '.$invoice->number);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $invoice = $this->Invoices->patchEntity($invoice, $this->request->getData());
            if ($this->Invoices->save($invoice)) {
                $this->Flash->success(__('The invoice has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The invoice could not be saved. Please, try again.'));
        }
        $this->set(compact('invoice'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Invoice id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $invoice = $this->Invoices->get($id);
        if ($this->Invoices->delete($invoice)) {
            $this->Flash->success(__('The invoice has been deleted.'));
        } else {
            $this->Flash->error(__('The invoice could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
