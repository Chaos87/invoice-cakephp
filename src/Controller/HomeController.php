<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Invoice;
use App\Model\Table\InvoicesTable;

/**
 * Home Controller
 *
 *
 * @method \App\Model\Entity\Home[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 *
 */
class HomeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->set('title', 'DASHBOARD');
        $this->loadModel('Invoices');
        /** @var InvoicesTable $invoices */
        $invoices = $this->Invoices;
        $invoicesNumber = $invoices->find();
        $invoicesNumber->select(['date',
            'count' => $invoicesNumber->func()->count('*')
            ]
        )->where(function($exp){
            return $exp->between('date', (new \DateTime())->add(\DateInterval::createFromDateString('-30 day')),new \DateTime() );
        })->group('date');
        $data = [];
        $max = 0;
        /** @var Invoice $row */
        foreach ($invoicesNumber as $row) {
            $data['labels'][] = $row->getDate();
            $data['series'][0][] = $row->count;
            $max = ($row->count > $max)? $row->count : $max;
        }
        $this->set('data', $data);
        $this->set('max', $max);
        $this->set('_serialize', ['data']);
    }

}
