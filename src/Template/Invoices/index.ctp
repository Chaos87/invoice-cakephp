<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Invoice[]|\Cake\Collection\CollectionInterface $invoices
 */

use Cake\Routing\Router; ?>
<div class="row">
    <div class="col-md-12">
        <div class="card card-plain">
            <div class="card-header bg-light">
                <h4 class="card-title mt-3"> Lista faktur</h4>
            </div>
            <a href="<?= Router::url(['_name' => 'add']); ?>" ><button type="button" class="btn btn-success">Dodaj</button></a>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="myTable" class="table table-hover" data-page-length='5'>
                        <thead class="">
                            <th scope="col">LP.</th>
                            <th scope="col">Numer</th>
                            <th scope="col">Data</th>
                            <th scope="col">Kwota</th>
                            <th scope="col">Nazwa Firmy</th>
                            <th scope="col">Adres</th>
                            <th scope="col">Email</th>
                            <th scope="col">Akcja</th>
                        </thead>
                        <tbody>
                            <?php foreach ($invoices as $invoice): ?>
                                <tr>
                                    <td><?= $this->Number->format($invoice->id) ?></td>
                                    <td><?= h($invoice->number) ?></td>
                                    <td><?= $invoice->getDate() ?></td>
                                    <td><?= $this->Number->format($invoice->amount, ['precision' => 2]) ?></td>
                                    <td><?= h($invoice->name_company) ?></td>
                                    <td><?= h($invoice->address) ?></td>
                                    <td><?= h($invoice->email) ?></td>
                                    <td class="actions">
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('datatables.min.js') ?>
<script>
    $(document).ready( function () {
        var dataTable = $('#myTable').DataTable({
            "pageLength": 5,
            "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
            searching: false,
            "pagingType": "full_numbers",
            "processing": true,
            "serverSide": true,
            'ajax' : {
                "url" : '<?= Router::url(['_name' => 'ajaxIndex']); ?>',
                "dataSrc": "invoices",
                type: "post",
            },
            columns : [
                { "data": "id" },
                { "data": "number" },
                { "data": null, render:  function(data, type, full){
                    return data.date.substr(0,10);
                    } },
                { "data": "amount" },
                { "data": "name_company" },
                { "data": "address" },
                { "data": "email" },
                { "data" : null, render: function(data, type, full) {
                    return '<a href="#" data-id="'+data.id+'" class="removeRow"><i class="material-icons">delete_forever</i></a>'+
                        '<a href="/invoice/edit/'+data.id+'" class="editRow"><i class="material-icons">edit</i></a>'+
                        '<a href="/invoice/view/'+data.id+'" class="showRow"><i class="material-icons">search</i></a>'
                    }}
            ],
            "columnDefs": [ {
                "targets": 7,
                "orderable": false
            } ]
        });

        $(document).on('click', '.removeRow', function(){
           let id = $(this).data('id');
           if(confirm('Czy na pewno usunąć?')){
               $.ajax({
                   url : '/invoice/delete/'+id,
                   type: "post",
                   success: function(data){
                       dataTable.ajax.reload();
                   },
                   error: function () {
                       alert('error');
                   }
               })
           }else{
               return false;
           }
        });



    } );
</script>
