<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Invoice[]|\Cake\Collection\CollectionInterface $invoices
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="card card-plain">
            <div class="card-header card-header-primary">
                <h4 class="card-title mt-0"> Lista faktur</h4>
                <p class="card-category"> Here is a subtitle for this table</p>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="">
                            <th scope="col"><?= $this->Paginator->sort('id', 'LP.') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('number', 'Numer') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('date', 'Data') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('amount', 'Kwota') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('name_company', 'Nazwa Firmy') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('address', 'Adres') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('email', 'Email') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </thead>
                        <tbody>
                            <?php foreach ($invoices as $invoice): ?>
                                <tr>
                                    <td><?= $this->Number->format($invoice->id) ?></td>
                                    <td><?= h($invoice->number) ?></td>
                                    <td><?= h($invoice->date) ?></td>
                                    <td><?= $this->Number->format($invoice->amount) ?></td>
                                    <td><?= h($invoice->name_company) ?></td>
                                    <td><?= h($invoice->address) ?></td>
                                    <td><?= h($invoice->email) ?></td>
                                    <td class="actions">
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
    </div>
</div>
<?= $this->Html->script('datatables.min.js') ?>
