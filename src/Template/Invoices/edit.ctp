<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Invoice $invoice
 */
?>
    <?= $this->Form->create($invoice) ?>
    <fieldset>
        <?php
            echo $this->Form->control('number');
            echo $this->Form->control('date');
            echo $this->Form->control('amount');
            echo $this->Form->control('name_company');
            echo $this->Form->control('address');
            echo $this->Form->control('email');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>

