<?= $this->Html->script('chartist.min.js') ?>

<div class="ct-chart"></div>

<script>
    var data = <?= json_encode($data) ?>;

    var options = {
        high: '<?= $max ?>',
        axisY: {
            onlyInteger: true
        },
    };

    new Chartist.Bar('.ct-chart', data, options);
</script>
