<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'NAZWA';

use Cake\Routing\Router; ?>
<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">
<head>
    <?= $this->Html->charset() ?>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        <?= $cakeDescription ?>:
        <?= $title ?>
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <?= $this->Html->css('material-dashboard.css') ?>
    <?= $this->Html->css('chartist.min.css') ?>


    <?= $this->Html->css('datatables.min.css') ?>


    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>


</head>
<body class="">
<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="/img/sidebar-1.jpg">
        <div class="logo">
            <a href="<?= Router::url(['_name' => 'home']); ?>" class="simple-text logo-normal">
                <?= $cakeDescription ?>
            </a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="nav-item<?= $this->request->getParam('_name') == 'home'? ' active' : ' ' ?>">
                    <a class="nav-link" href="<?= Router::url(['_name' => 'home']); ?>">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item<?= $this->request->getParam('_name') == 'invoice'? ' active' : ' ' ?>">
                    <a class="nav-link" href="<?= Router::url(['_name' => 'invoice']); ?>">
                        <i class="material-icons">content_paste</i>
                        <p>Faktury</p>
                    </a>
                </li>
                <li class="nav-item<?= $this->request->getParam('_name') == 'setting'? ' active' : ' ' ?>">
                    <a class="nav-link" href="<?= Router::url(['_name' => 'setting']); ?>">
                        <i class="material-icons">settings</i>
                        <p>Ustawienia</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper"><?= $title ?>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="/menu" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">person</i>
                                <p class="d-lg-none d-md-block">
                                    Account
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                <a class="dropdown-item" href="#">Ustawienia</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Log out</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <?= $this->Html->script('core/jquery.min.js') ?>
        <?= $this->Html->script('core/popper.min.js') ?>
        <?= $this->Html->script('core/bootstrap-material-design.min.js') ?>
        <?= $this->Html->script('plugins/perfect-scrollbar.jquery.min.js') ?>
        <div class="content">
            <?= $this->Flash->render() ?>
            <div class="container-fluid">
                <?= $this->fetch('content') ?>
            </div>
        </div>
    </div>
</div>
    <footer>
    </footer>

<?= $this->Html->script('sidebar.js') ?>
</body>
</html>
