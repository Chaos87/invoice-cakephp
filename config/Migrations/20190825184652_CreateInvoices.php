<?php
use Migrations\AbstractMigration;

class CreateInvoices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        /** @var \Migrations\Table $table */
        $table = $this->table('invoices');
        $table->addColumn('number', 'string',[
            'limit' => 255,
            'null' => false
        ]);
        $table->addIndex(['number'], ['type' => 'unique']);

        $table->addColumn('date', 'date',[
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('amount', 'decimal',[
            'null' => false,
            'precision' => 15,
            'scale' => 2,
        ]);

        $table->addColumn('name_company', 'string',[
            'null' => false,
        ]);

        $table->addColumn('address', 'string');

        $table->addColumn('email', 'string');

        $table->create();
    }
}
