<?php


use Phinx\Seed\AbstractSeed;

class InvoicesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $beforeMonth = 2;
        $maxInvoiceInDay = 5;

        for($i=$beforeMonth;$i>=0;$i--)
        {
            $invoicesDate = (new \DateTime())->add(\DateInterval::createFromDateString("-$i month"));
            $invoiceNumber = $invoicesDate->format('Y').'/'.$invoicesDate->format('m').'/';
            $invoiceCounter = 1;
            if($i==0){
                $lastDayInMonth = date('d');
            }else{
                $lastDayInMonth = date('t', strtotime($invoicesDate->format('Y-m-01')));
            }
            for($j=1;$j<=$lastDayInMonth; $j++){
                $invoiceInDay = rand(0, $maxInvoiceInDay);
                for($k=0;$k<$invoiceInDay;$k++)
                {
                    $data[] = [
                        'number'    => $invoiceNumber.$invoiceCounter,
                        'date' => $invoicesDate->format('Y-m')."-$j",
                        'amount' => rand(30000, 1200000)/100,
                        'name_company' => $faker->company,
                        'address' => $faker->address,
                        'email' => $faker->companyEmail
                    ];
                    $invoiceCounter++;
                }
            }
        }
        $posts = $this->table('invoices');
        $posts->insert($data)
            ->save();
    }
}
